package com.example.ivametal.deadprojecttwo.connection

import android.util.Base64
import android.util.Log

import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response

import java.io.IOException

/**
 * Created by ivametal on 10.04.17.
 */
class ServerConnector private constructor() {
    private var str = ""
    internal var client: OkHttpClient

    init {
        client = OkHttpClient()
    }

    fun getJsonRequest(url: String): String {
        val request = Request.Builder().url(url).build()
        try {
            str = client.newCall(request).execute().body().string()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return str
    }

    fun postJsonResponse(json: String): Boolean {
        val base64Password = Base64.encodeToString("stakan".toByteArray(), Base64.NO_WRAP)
        val JSON = MediaType.parse("application/json, charset=utf-8")
        val client = OkHttpClient()
        val requestBody = RequestBody.create(JSON, json)
        val request = Request.Builder()
                .url("http://192.168.0.28:9999/post")
                .addHeader("Authorization", base64Password)
                .put(requestBody)
                .build()
        try {
            val response = client.newCall(request).execute()
        } catch (e: IOException) {
            e.printStackTrace()
            return false
        }

        return true
    }

    companion object {

        val serverConnector = ServerConnector()
    }
}
