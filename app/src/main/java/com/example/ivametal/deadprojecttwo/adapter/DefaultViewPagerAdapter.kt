package com.example.ivametal.deadprojecttwo.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import com.example.ivametal.deadprojecttwo.fragments.DefaultFragment
import com.example.ivametal.deadprojecttwo.model.Game

import java.util.ArrayList

/**
 * Created by ivametal on 11/05/17.
 */
class DefaultViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
    private val fragments = ArrayList<Fragment>()
    private val defaultFragmentTitle = ArrayList<String>()

    init {
        fragments.add(DefaultFragment())
        fragments.add(DefaultFragment())
        defaultFragmentTitle.add("Скидки")
        defaultFragmentTitle.add("Обмен")
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return defaultFragmentTitle[position]
    }
}
