package com.example.ivametal.deadprojecttwo.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.ivametal.deadprojecttwo.R

/**
 * Created by ivametal on 11/05/17.
 */
class DefaultFragment : android.support.v4.app.Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_default, container, false)
        return view

    }
}
