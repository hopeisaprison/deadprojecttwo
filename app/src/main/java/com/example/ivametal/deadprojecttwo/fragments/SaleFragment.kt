package com.example.ivametal.deadprojecttwo.fragments

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar

import com.example.ivametal.deadprojecttwo.R
import com.example.ivametal.deadprojecttwo.adapter.SaleRecyclerViewAdapter
import com.example.ivametal.deadprojecttwo.connection.ServerConnector
import com.example.ivametal.deadprojecttwo.model.Game
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

import java.util.ArrayList

/**
 * Created by ivametal on 10.04.17.
 */
class SaleFragment : android.support.v4.app.Fragment() {

    private var recyclerView: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var searchView: android.support.v7.widget.SearchView? = null
    private var games: ArrayList<Game>? = null
    private var refreshLayout: SwipeRefreshLayout? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.recyclerview, container, false)
        recyclerView = view.findViewById(R.id.recycler_fragment) as RecyclerView
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        progressBar = view.findViewById(R.id.progressbar) as ProgressBar
        refreshLayout = view.findViewById(R.id.swipe_to_refresh) as SwipeRefreshLayout
        searchView = activity.findViewById(R.id.action_search) as android.support.v7.widget.SearchView
        searchView!!.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                val foundGames = ArrayList<Game>()
                if (games == null)
                    return false
                for (i in games!!.indices)
                    if (games!![i].name.contains(newText))
                        foundGames.add(games!![i])
                recyclerView!!.adapter = SaleRecyclerViewAdapter(foundGames)
                return false
            }
        })

        val gameLoader = GameLoader(false)
        gameLoader.execute()

        refreshLayout!!.setOnRefreshListener { GameLoader(true).execute() }
        return view
    }

    private inner class GameLoader internal constructor(private val fromRefresh: Boolean) : AsyncTask<Void, ArrayList<Game>, ArrayList<Game>>() {
        internal var connectivityManager: ConnectivityManager
        internal var networkInfo: NetworkInfo? = null

        init {
            connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            networkInfo = connectivityManager.activeNetworkInfo
        }


        override fun onPostExecute(games: ArrayList<Game>) {
            if (networkInfo == null) {
                Snackbar.make(view!!, "No internet connection", Snackbar.LENGTH_LONG).setAction("Action", null).show()
                progressBar!!.visibility = View.INVISIBLE
                return
            }
            progressBar!!.visibility = View.INVISIBLE
            recyclerView!!.visibility = View.VISIBLE
            val adapter = SaleRecyclerViewAdapter(games)
            recyclerView!!.adapter = adapter
            if (fromRefresh)
                refreshLayout!!.isRefreshing = false
            super.onPostExecute(games)
        }


        override fun doInBackground(vararg voids: Void): ArrayList<Game> {
            games = ArrayList<Game>()
            val bundle = arguments
            val url = bundle!!.getString("URL")
            val json = ServerConnector.serverConnector.getJsonRequest(url)
            try {
                val jsonArray = JSONArray(json)
                for (i in 0..jsonArray.length() - 1) {
                    val jsonObject = jsonArray.getJSONObject(i)
                    val game = Game(jsonObject.getString("name"),
                            jsonObject.getString("sale"))
                    games!!.add(game)
                }

            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return games as ArrayList<Game>
        }
    }

    companion object {

        fun newInstance(args: Bundle): SaleFragment {
            val fragment = SaleFragment()
            fragment.arguments = args
            return fragment
        }
    }
}


