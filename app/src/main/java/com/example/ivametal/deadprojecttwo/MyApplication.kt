package com.example.ivametal.deadprojecttwo

import android.app.Application
import com.vk.sdk.VKSdk

/**
 * Created by ivametal on 02/05/17.
 */
class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        VKSdk.initialize(applicationContext)
    }
}
