package com.example.ivametal.deadprojecttwo

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.widget.TextView
import butterknife.bindView
import com.example.ivametal.deadprojecttwo.fragments.ExchangeFragment
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by ivametal on 01/07/17.
 */
class ExchangeInfoActivity : Activity() {
    private val headInfoText : TextView by bindView(R.id.activity_exchangeinfo_headtext)
    private val bodyInfoText : TextView by bindView(R.id.activity_exchangeinfo_bodytext)
    private val userNameText : TextView by bindView(R.id.activity_exchangeinfo_username)
    private val usrImage : CircleImageView by bindView(R.id.activity_exchangeinfo_userimage)
    private val sendButton : FloatingActionButton by bindView(R.id.send_exchange_info_fab)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exchange_info)


        val bundle = intent.extras
        headInfoText.text = bundle.getString(ExchangeFragment.CONSTANTS.HEADTEXT)
        bodyInfoText.text = bundle.getString(ExchangeFragment.CONSTANTS.BODYTEXT)
        userNameText.text = bundle.getString(ExchangeFragment.CONSTANTS.USERNAME)

        Picasso.with(this)
                .load(bundle.getString(ExchangeFragment.CONSTANTS.USERIMAGEURL))
                .into(usrImage)


        sendButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, "123123")
            startActivity(intent)
        }

    }
}

