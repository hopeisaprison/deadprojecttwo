package com.example.ivametal.deadprojecttwo.adapter

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.ivametal.deadprojecttwo.R
import com.example.ivametal.deadprojecttwo.model.Game

import java.util.ArrayList

/**
 * Created by ivametal on 10.04.17.
 */
internal class SaleRecyclerViewAdapter(private val games: ArrayList<Game>) : RecyclerView.Adapter<SaleRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cardview_sale, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.nameText.text = games[position].name
        holder.saleText.text = games[position].sale
    }

    override fun getItemCount(): Int {
        Log.d("TAG", "312 " + games.size)
        return games.size
    }

    internal inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameText: TextView
        var saleText: TextView

        init {
            nameText = itemView.findViewById(R.id.gamename_card) as TextView
            saleText = itemView.findViewById(R.id.salenumber_card) as TextView
        }
    }

    /**
     * Created by ivametal on 10.04.17.
     */
    //    public static class ImgTransformator implements Transformation {
    //
    //        private Context context;
    //        ImageView imageView;
    //
    //        public ImgTransformator(Context context, ImageView imageView) {
    //            this.imageView = imageView;
    //            this.context = context;
    //        }
    //
    //
    //        @Override
    //        public Bitmap transform(Bitmap source) {
    //            Resources res = context.getResources();
    //            int x = (int) TypedValue.applyDimension
    //                    (TypedValue.COMPLEX_UNIT_DIP, 320,
    //                    res.getDisplayMetrics());
    //            int y = (int) TypedValue.applyDimension
    //                    (TypedValue.COMPLEX_UNIT_DIP, 340,
    //                    res.getDisplayMetrics());
    //            Bitmap transformBitmap = Bitmap.createBitmap(source,source.getWidth()/4
    //                                                         ,source.getHeight()/4,
    //                                                          x,y);
    //            source.recycle();
    //            return transformBitmap;
    //        }
    //
    //        @Override
    //        public String key() {
    //            return "AHAHA ";
    //        }
    //    }
}


