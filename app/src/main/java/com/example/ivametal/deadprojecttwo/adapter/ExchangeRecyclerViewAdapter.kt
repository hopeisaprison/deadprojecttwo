package com.example.ivametal.deadprojecttwo.adapter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.example.ivametal.deadprojecttwo.ExchangeInfoActivity

import com.example.ivametal.deadprojecttwo.MainActivity
import com.example.ivametal.deadprojecttwo.R
import com.example.ivametal.deadprojecttwo.connection.ServerConnector
import com.example.ivametal.deadprojecttwo.fragments.ExchangeFragment
import com.example.ivametal.deadprojecttwo.model.ExchangeRow
import com.squareup.picasso.Picasso


import org.json.JSONException
import org.json.JSONObject

import java.util.ArrayList

import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by ivametal on 19/05/17.
 */

internal class ExchangeRecyclerViewAdapter(private val exchangeRows: ArrayList<ExchangeRow>, private val context: Context) : RecyclerView.Adapter<ExchangeRecyclerViewAdapter.ViewHolder>() {
    private var preferences: SharedPreferences? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.cardview_exchange, parent, false)
        preferences = context.getSharedPreferences(MainActivity.VK_PREFERENCES, Context.MODE_PRIVATE)
        return ViewHolder(view)
    }



    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //        holder.date.setText(exchangeRows.get(position).getDate());
        //        holder.mainText.setText(exchangeRows.get(position).getMainText());
        if (preferences!!.getString(MainActivity.USR_ID, null) == exchangeRows[position].vkId)
            holder.deleteButton.visibility = View.VISIBLE
        Log.d("VKID", preferences!!.getString(MainActivity.USR_ID, null))
        Log.d("VKID", exchangeRows[position].vkId)
        holder.username.text = exchangeRows[position].vkUserName
        holder.deleteButton.setOnClickListener {
            val jsonObject = JSONObject()
            try {
                jsonObject.put("remove", 1)
                jsonObject.put("vkid", exchangeRows[position].vkId)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            DeleteRequest().execute(jsonObject.toString(), position.toString())
        }
        Picasso.with(context)
                .load(exchangeRows[position].vkUserImgUrl)
                .into(holder.circleImageView)
    }

    override fun getItemCount(): Int {
        return exchangeRows.size
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var deleteButton: ImageButton
        var mainText: TextView
        var username: TextView
        var date: TextView
        var circleImageView: CircleImageView

        init {
            deleteButton = itemView.findViewById(R.id.delete_imagebutton) as ImageButton
            mainText = itemView.findViewById(R.id.head_exchange_textview) as TextView
            username = itemView.findViewById(R.id.vk_username_textview) as TextView
            date = itemView.findViewById(R.id.date_text_exchange_textview) as TextView
            circleImageView = itemView.findViewById(R.id.vk_userimg_imageview) as CircleImageView
            itemView.setOnClickListener {
                val intent = Intent(context!!, ExchangeInfoActivity::class.java)
                val bundle = Bundle()
                println(exchangeRows[adapterPosition].headText)
                bundle.putString(ExchangeFragment.CONSTANTS.HEADTEXT, exchangeRows[adapterPosition].headText)
                bundle.putString(ExchangeFragment.CONSTANTS.BODYTEXT, exchangeRows[adapterPosition].mainText)
                bundle.putString(ExchangeFragment.CONSTANTS.USERID, exchangeRows[adapterPosition].vkId)
                bundle.putString(ExchangeFragment.CONSTANTS.USERNAME, exchangeRows[adapterPosition].vkUserName)
                bundle.putString(ExchangeFragment.CONSTANTS.USERIMAGEURL, exchangeRows[adapterPosition].vkUserImgUrl)
                intent.putExtras(bundle)
                context.startActivity(intent)
            }
        }
    }

    private inner class DeleteRequest : AsyncTask<String, Void, Int>() {
        override fun onPostExecute(aInteger: Int?) {
            Toast.makeText(context, "Successfull", Toast.LENGTH_SHORT).show()
            notifyDataSetChanged()
        }

        override fun doInBackground(vararg params: String): Int? {
            val serverConnector = ServerConnector.serverConnector
            serverConnector.postJsonResponse(params[0])
            exchangeRows.removeAt(Integer.parseInt(params[1]))
            val temp = ArrayList<ExchangeRow>()
            temp.addAll(exchangeRows)
            exchangeRows.clear()
            exchangeRows.addAll(temp)
            return Integer.valueOf(params[1])
        }
    }


}
