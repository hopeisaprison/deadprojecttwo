package com.example.ivametal.deadprojecttwo.adapter

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.util.Log

import com.example.ivametal.deadprojecttwo.fragments.DefaultFragment
import com.example.ivametal.deadprojecttwo.fragments.ExchangeFragment
import com.example.ivametal.deadprojecttwo.fragments.SaleFragment

import java.util.ArrayList

/**
 * Created by ivametal on 11/05/17.
 */
class ViewPagerAdapter(fm: FragmentManager, private val bundle: Bundle) : FragmentStatePagerAdapter(fm) {
    private val defaultFragmentTitle = ArrayList<String>()

    init {
        defaultFragmentTitle.add("Скидки")
        defaultFragmentTitle.add("Обмен")
    }

    override fun getPageTitle(position: Int): CharSequence {
        return defaultFragmentTitle[position]
    }

    override fun getItem(position: Int): Fragment {
        Log.d("DEBUG", position.toString() + "")
        if (position == 0)
            return SaleFragment.newInstance(bundle)
        else {
            val exchangeFragment = ExchangeFragment()
            exchangeFragment.arguments = bundle
            return exchangeFragment
        }
    }

    override fun getCount(): Int {
        return 2
    }
}
