package com.example.ivametal.deadprojecttwo.fragments

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar

import com.example.ivametal.deadprojecttwo.ExchangeActivity
import com.example.ivametal.deadprojecttwo.ExchangeInfoActivity
import com.example.ivametal.deadprojecttwo.MainActivity
import com.example.ivametal.deadprojecttwo.R
import com.example.ivametal.deadprojecttwo.adapter.ExchangeRecyclerViewAdapter
import com.example.ivametal.deadprojecttwo.connection.ServerConnector
import com.example.ivametal.deadprojecttwo.model.ExchangeRow
import com.vk.sdk.VKSdk

import org.json.JSONArray
import org.json.JSONException

import java.util.ArrayList

/**
 * Created by ivametal on 11/05/17.
 */
class ExchangeFragment : android.support.v4.app.Fragment() {

    object CONSTANTS {
        public val HEADTEXT = "HEADTEXT"
        public val BODYTEXT = "BODYTEXT"
        public val USERID = "USERID"
        public val USERNAME = "USERNAME"
        public val USERIMAGEURL = "USRIMAGEURL"
    }

    private var exchangeRows: ArrayList<ExchangeRow>? = null
    private var recyclerView: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var refreshLayout: SwipeRefreshLayout? = null
    private var searchView: SearchView? = null
    private var fab: FloatingActionButton? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.recyclerview, container, false)
        recyclerView = view.findViewById(R.id.recycler_fragment) as RecyclerView
        recyclerView!!.layoutManager = LinearLayoutManager(context)


        progressBar = view.findViewById(R.id.progressbar) as ProgressBar
        refreshLayout = view.findViewById(R.id.swipe_to_refresh) as SwipeRefreshLayout
        refreshLayout!!.setOnRefreshListener { ExchangeLoader(true).execute() }

        fab = activity.findViewById(R.id.fab) as FloatingActionButton
        fab!!.setOnClickListener { v ->
            if (VKSdk.isLoggedIn()) {
                val intent = Intent(context, ExchangeActivity::class.java)
                intent.putExtras(arguments)
                startActivityForResult(intent, 0)
            } else
                Snackbar.make(v, "Please, login in VK", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null)
                        .show()
        }

        searchView = activity.findViewById(R.id.action_search) as android.support.v7.widget.SearchView
        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                val exchanges = ArrayList<ExchangeRow>()
                if (exchangeRows == null)
                    return false
                for (i in exchangeRows!!.indices)
                    if (exchangeRows!![i].headText.contains(newText))
                        exchanges.add(exchangeRows!![i])
                recyclerView!!.adapter = ExchangeRecyclerViewAdapter(exchanges, context)
                return false
            }
        })
        ExchangeLoader(false).execute()

        return view


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        ExchangeLoader(false).execute()
    }

    private inner class ExchangeLoader internal constructor(private val fromRefresh: Boolean) : AsyncTask<Void, ArrayList<ExchangeRow>, ArrayList<ExchangeRow>>() {
        internal var connectivityManager: ConnectivityManager
        internal var networkInfo: NetworkInfo? = null

        init {
            connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            networkInfo = connectivityManager.activeNetworkInfo
        }

        override fun doInBackground(vararg params: Void): ArrayList<ExchangeRow> {
            val serverConnector = ServerConnector.serverConnector
            val json = serverConnector.getJsonRequest(MainActivity.EXCHANGES_URL)
            exchangeRows = ArrayList<ExchangeRow>()
            try {
                val jsonArray = JSONArray(json)
                for (i in 0..jsonArray.length() - 1) {
                    val jsonObject = jsonArray.getJSONObject(i)
                    val exchangeRow = ExchangeRow(
                            jsonObject.getString("vkId"),
                            jsonObject.getString("userName"),
                            jsonObject.getString("usrImgUrl"),
                            jsonObject.getString("headPost"),
                            jsonObject.getString("bodyPost"),
                            jsonObject.getString("platformUrl"))
                    if (exchangeRow.platformUrl == arguments.getString("URL"))
                        exchangeRows!!.add(exchangeRow)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return exchangeRows as ArrayList<ExchangeRow>
        }

        override fun onPostExecute(exchangeRows: ArrayList<ExchangeRow>) {
            if (networkInfo == null) {
                Snackbar.make(view!!, "No internet connection", Snackbar.LENGTH_LONG).setAction("Action", null).show()
                progressBar!!.visibility = View.INVISIBLE
                return
            }
            progressBar!!.visibility = View.INVISIBLE
            recyclerView!!.visibility = View.VISIBLE
            val adapter = ExchangeRecyclerViewAdapter(exchangeRows, context)
            recyclerView!!.adapter = adapter
            if (fromRefresh)
                refreshLayout!!.isRefreshing = false
            super.onPostExecute(exchangeRows)
        }
    }
}
