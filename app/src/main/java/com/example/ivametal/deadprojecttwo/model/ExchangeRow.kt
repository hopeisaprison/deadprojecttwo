package com.example.ivametal.deadprojecttwo.model

import okhttp3.internal.platform.Platform

/**
 * Created by ivametal on 19/05/17.
 */

class ExchangeRow(val vkId: String, val vkUserName: String, val vkUserImgUrl: String, val headText: String, val mainText: String, val platformUrl: String) {

    override fun toString(): String {
        return vkId
    }
}
