package com.example.ivametal.deadprojecttwo

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.View
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.ivametal.deadprojecttwo.adapter.DefaultViewPagerAdapter
import com.example.ivametal.deadprojecttwo.adapter.ViewPagerAdapter
import com.squareup.picasso.Picasso
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKCallback
import com.vk.sdk.VKScope
import com.vk.sdk.VKSdk
import com.vk.sdk.api.*
import com.vk.sdk.api.model.VKApiUserFull
import com.vk.sdk.api.model.VKList

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private var searchView: SearchView? = null
    private var usrIconImg: ImageView? = null
    private var usrNameTxt: TextView? = null
    private var viewPager: ViewPager? = null
    private var tabLayout: TabLayout? = null
    private var fab: FloatingActionButton? = null
    private var editor: SharedPreferences.Editor? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        editor = applicationContext.getSharedPreferences(VK_PREFERENCES, Context.MODE_PRIVATE).edit()
        searchView = findViewById(R.id.action_search) as SearchView
        searchView!!.visibility = View.INVISIBLE
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        viewPager = findViewById(R.id.viewpager) as ViewPager
        viewPager!!.adapter = DefaultViewPagerAdapter(supportFragmentManager)

        tabLayout = findViewById(R.id.tablayout) as TabLayout
        tabLayout!!.setupWithViewPager(viewPager)

        fab = findViewById(R.id.fab) as FloatingActionButton

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)

        val headerView = navigationView.getHeaderView(0)

        usrNameTxt = headerView.findViewById(R.id.textView) as TextView

        usrIconImg = headerView.findViewById(R.id.imageView) as ImageView
        usrIconImg!!.setOnClickListener { v -> VKSdk.login(this@MainActivity, *scopes) }

        if (VKSdk.isLoggedIn()) {
            val preferences = applicationContext.getSharedPreferences(VK_PREFERENCES, Context.MODE_PRIVATE)
            Picasso.with(this)
                    .load(preferences.getString(USR_IMGURL_KEY, null))
                    .into(this@MainActivity.usrIconImg)
            usrNameTxt!!.text = preferences.getString(USERNAME_KEY, null)
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, object : VKCallback<VKAccessToken> {
            override fun onResult(res: VKAccessToken?) {
                if (res != null) {
                    Snackbar.make(this@MainActivity.currentFocus!!, "Login successful", Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show()
                    var request = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, "photo_100"))
                    request.executeWithListener(object : VKRequest.VKRequestListener() {
                        override fun onComplete(response: VKResponse) {
                            super.onComplete(response)
                            val user = (response.parsedModel as VKList<VKApiUserFull>)[0]
                            editor!!.putString(USR_IMGURL_KEY, user.photo_100)
                            editor!!.commit()
                            Picasso.with(this@MainActivity)
                                    .load(user.photo_100)
                                    .into(this@MainActivity.usrIconImg)

                        }
                    })

                    request = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, "first_name"))
                    request.executeWithListener(object : VKRequest.VKRequestListener() {
                        override fun onComplete(response: VKResponse) {
                            super.onComplete(response)
                            val user = (response.parsedModel as VKList<VKApiUserFull>)[0]
                            editor!!.putString(USERNAME_KEY, user.toString())
                            editor!!.putString(USR_ID, user.id.toString())
                            editor!!.commit()
                            usrNameTxt!!.text = user.toString()
                        }
                    })


                }
            }

            override fun onError(error: VKError) {

            }
        }))

            super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)

        val searchView = findViewById(R.id.action_search) as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                Toast.makeText(this@MainActivity, "WOOOAH", Toast.LENGTH_SHORT).show()
                return false
            }
        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val bundle = Bundle()
        when (item.itemId) {
            R.id.nav_psThree -> bundle.putString("URL", PSTHREE_URL)
            R.id.nav_psFour -> bundle.putString("URL", PSFOUR_URL)
            R.id.nav_Steam -> bundle.putString("URL", STEAM_URL)
            R.id.nav_Xbox -> bundle.putString("URL", XBOX_URL)
            else -> {
            }
        }


        viewPager!!.adapter = ViewPagerAdapter(supportFragmentManager, bundle)
        viewPager!!.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                Log.d("PAGE", position.toString() + "")
                if (position == 1)
                    fab!!.show()
                else
                    fab!!.hide()

            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
        searchView!!.visibility = View.VISIBLE


        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    companion object {

        val VK_PREFERENCES = "com.example.ivametal.deadprojecttwo.VK_PREFERENCE"
        val USERNAME_KEY = "username"
        val USR_IMGURL_KEY = "usrimgurl"
        val USR_ID = "usrid"
        private val PSFOUR_URL = "http://192.168.0.28:9999/psFour"
        private val PSTHREE_URL = "http://192.168.0.28:9999/psThree"
        private val XBOX_URL = "http://192.168.0.28:9999/xbox"
        private val STEAM_URL = "http://192.168.0.28:9999/steam"
        val EXCHANGES_URL = "http://192.168.0.28:9999/exchanges"
        private val scopes = arrayOf(VKScope.FRIENDS, VKScope.PHOTOS, VKScope.STATS)
    }
}
