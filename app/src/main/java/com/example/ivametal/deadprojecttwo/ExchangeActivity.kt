package com.example.ivametal.deadprojecttwo

import android.content.Context
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.Toast

import com.example.ivametal.deadprojecttwo.connection.ServerConnector

import org.json.JSONException
import org.json.JSONObject

/**
 * Created by ivametal on 28/05/17.
 */

class ExchangeActivity : AppCompatActivity() {

    private var headPost: EditText? = null
    private var bodyPost: EditText? = null
    private var fabExchange: FloatingActionButton? = null
    private var sharedPreferences: SharedPreferences? = null
    private var frameLayout: FrameLayout? = null
    private var relativeLayout: RelativeLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_exchange)
        sharedPreferences = applicationContext.getSharedPreferences(MainActivity.VK_PREFERENCES, Context.MODE_PRIVATE)

        relativeLayout = findViewById(R.id.exchange_main_layout) as RelativeLayout
        frameLayout = findViewById(R.id.exchange_frame_layout) as FrameLayout
        fabExchange = findViewById(R.id.fab_exchange) as FloatingActionButton
        headPost = findViewById(R.id.exchange_head_edit) as EditText
        bodyPost = findViewById(R.id.exchange_body_edit) as EditText


        fabExchange!!.setOnClickListener { view ->
            val jsonObject = JSONObject()
            try {
                jsonObject.put("head", headPost!!.text.toString())
                jsonObject.put("body", bodyPost!!.text.toString())
                jsonObject.put("username", sharedPreferences!!.getString(MainActivity.USERNAME_KEY, null))
                jsonObject.put("imgurl", sharedPreferences!!.getString(MainActivity.USR_IMGURL_KEY, null))
                jsonObject.put("vkid", sharedPreferences!!.getString(MainActivity.USR_ID, null))
                jsonObject.put("remove", 0)
                jsonObject.put("platformurl", intent.getStringExtra("URL"))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            relativeLayout!!.alpha = 0.5f
            frameLayout!!.visibility = View.VISIBLE
            PostLoader().execute(jsonObject.toString())
        }
    }


    private inner class PostLoader : AsyncTask<String, Void, Boolean>() {


        override fun onPostExecute(aBoolean: Boolean?) {
            if (aBoolean == true) {
                Toast.makeText(baseContext, "Complete", Toast.LENGTH_SHORT).show()
                setResult(1)
                finish()
            }
            if (aBoolean != true) {
                Toast.makeText(baseContext, "Try once", Toast.LENGTH_SHORT).show()
                frameLayout!!.visibility = View.INVISIBLE
                relativeLayout!!.alpha = 1f
            }
        }

        override fun doInBackground(vararg params: String): Boolean? {
            val serverConnector = ServerConnector.serverConnector
            return serverConnector.postJsonResponse(params[0])
        }
    }
}
